package com.devcamp.task60_40.car.service;

import java.util.ArrayList;

import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.task60_40.car.model.car;
import com.devcamp.task60_40.car.model.carType;
import com.devcamp.task60_40.car.repository.ICarRepository;

@Service
public class CarService {
  @Autowired
  ICarRepository pCarRepository;

  public ArrayList<car> getAllCars() {
    ArrayList<car> listCar = new ArrayList<>();
    pCarRepository.findAll().forEach(listCar::add);
    return listCar;
  }

  public Set<carType> getCarTypeByCarCode(String carCode) {
    car vCar = pCarRepository.findByCarCode(carCode);
    if (vCar != null) {
      return vCar.getTypes();
    } else {
      return null;
    }
  }

}
