package com.devcamp.task60_40.car.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task60_40.car.model.car;

public interface ICarRepository extends JpaRepository<car, Long> {
  car findByCarCode(String carCode);
}
