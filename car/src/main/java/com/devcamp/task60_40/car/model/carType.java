package com.devcamp.task60_40.car.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "car_types")
public class carType {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int carTypeId;

    @Column(name = "type_car", unique = true)
    private String typeCar;

    @Column(name = "type_name")
    private String typeName;

    @ManyToOne
    @JoinColumn(name = "car_id")
    private car car;
    
    public carType() {
    }

    public carType(int carTypeId, String typeCar, String typeName, com.devcamp.task60_40.car.model.car car) {
        this.carTypeId = carTypeId;
        this.typeCar = typeCar;
        this.typeName = typeName;
        this.car = car;
    }

    public int getCarTypeId() {
        return carTypeId;
    }

    public void setCarTypeId(int carTypeId) {
        this.carTypeId = carTypeId;
    }

    public String getTypeCar() {
        return typeCar;
    }

    public void setTypeCar(String typeCar) {
        this.typeCar = typeCar;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

        
    }
