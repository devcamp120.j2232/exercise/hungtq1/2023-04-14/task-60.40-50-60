package com.devcamp.task60_40.car.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task60_40.car.model.carType;

public interface ICarTypeRepository extends JpaRepository<carType, Long> {

}
