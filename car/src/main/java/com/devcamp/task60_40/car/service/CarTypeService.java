package com.devcamp.task60_40.car.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.task60_40.car.model.carType;
import com.devcamp.task60_40.car.repository.ICarTypeRepository;

@Service
public class CarTypeService {
  @Autowired
  ICarTypeRepository pCarTypeRepository;

  public ArrayList<carType> getAllCarType() {
    ArrayList<carType> pCCarTypes = new ArrayList<>();
    pCarTypeRepository.findAll().forEach(pCCarTypes::add);
    return pCCarTypes;
  }
}