package com.devcamp.task60_40.car.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "cars")
public class car {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int carId;

    @Column(name = "car_code", unique = true)
    private String carCode;

    @Column(name = "car_name")
    private String carName;

    @OneToMany(mappedBy = "car", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<carType> types;
    
    public car() {
    }

    public car(int carId, String carCode, String carName, Set<carType> types) {
        this.carId = carId;
        this.carCode = carCode;
        this.carName = carName;
        this.types = types;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public String getCarCode() {
        return carCode;
    }

    public void setCarCode(String carCode) {
        this.carCode = carCode;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public Set<carType> getTypes() {
        return types;
    }

    public void setTypes(Set<carType> types) {
        this.types = types;
    }

   

    

         
}
